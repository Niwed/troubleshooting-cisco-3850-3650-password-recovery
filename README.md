# Troubleshooting Cisco 3850/3650 password recovery

> 1. Console into the switch
> 2. Power cycle the switch
> 3. Press and hold the Mode button until it glows amber
> 4. When in the boot loader mode ("Switch:" prompt): 
>    - initialize the flash system:
>    - Switch: **flash_init**
>    - load any helper files:
>    - Switch: **load_helper**
>    - display the content of the Flash location:
>    - Switch: **dir flash:**
>    - view the file system.
>    - Directory of flash:
>    - 13  drwx         192   Mar 01 1993 22:30:48 switch_image
>    - 11  -rwx        5825   Mar 01 1993 22:31:59  config.text
>    - 18  -rwx         720   Mar 01 1993 02:21:30  vlan.dat
>    - 16128000 bytes total (10003456 bytes free)
> 5. Create a new boot variable to bypass current STARTUP-CONFIG:
>    - Switch: **SWITCH_IGNORE_STARTUP_CFG=1**
> 6. Verify/Make sure the disable password recovery variable is not set to 1:
>    - Switch: **SWITCH_DISABLE_PASSWORD_RECOVERY=0**
> 7. Boot the switch using the image you want (e.g. flash: packages.conf  or flash: switch_image)
>    - Switch: **boot flash:packages.conf**      
>    - or   
>    - Switch: **boot flash:switch_image**    <---- image you want to use
> 8. When in IOS XE EXEC mode, update and save the updated startup config
>    - Switch> **enable**
>    - Switch# **copy startup-config to running config or**
>    - Switch# **config terminal** 
>    - Switch(config-if)# **enable secret password**
>    - Switch(config-if)# **end**
>    - Switch# **copy running-config startup-config**
> 9. Reload the switch
>    - Switch# **reload**
> 10. Break into boot loader one more time (step 3: Press and hold the Mode button until it glows amber)
> 11. When back in boot loader mode ("Switch:" prompt), remove the bypass startup config entry and reboot
> 12. Unset the variable:
>     - Switch: unset **SWITCH_IGNORE_STARTUP_CFG**
> 13. Boot the switch image as done before 
>     - Switch: boot flash:packages.conf      
>     - or
>     - Switch: **boot flash:switch_image**
